.PHONY: build-container

PROJECT_NAME=just-run-it
DOCKERIMAGE=punkstarman/just-run-it-prez

include lualatex.mk

build-container: $(shell find .build -type f)
	docker build -t $(DOCKERIMAGE) .build

push-build-container: build-container
	docker push $(DOCKERIMAGE)
