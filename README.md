# Prez

Slides for the presentation at 6th Docker Birthday

## Building

Requirements:

- A shell (ash, bash, dash, fish, ksh, ... or zsh)
- Docker 18.09.3
- GNU Make 4.1

On your favorite shell, run the following command.

```shell
make
```

If all goes well, a PDF should appear in the working directory.