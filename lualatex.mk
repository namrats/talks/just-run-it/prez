.DEFAULT_GOAL:=$(PROJECT_NAME).pdf

UID=$(shell id -u)
GID=$(shell id -g)

TEX=lualatex
TEXFLAGS=--interaction=nonstopmode --shell-escape --jobname=$(PROJECT_NAME)
LUATEX_CACHE=/tmp/.texlive2016
DOCKERFLAGS=-it --rm -u $(UID):$(GID) \
  -v $(LUATEX_CACHE):$(LUATEX_CACHE) \
	-v $(CURDIR)/fonts:/usr/local/share/fonts \
	-v $(CURDIR):/data \
	-w /data \
	-e HOME=/tmp

TEX_SCRIPT=for run in $$(seq 2) ; do $(TEX) $(TEXFLAGS) $< ; done

ifndef NO_DOCKER
$(LUATEX_CACHE):
	mkdir -p $@

$(PROJECT_NAME).pdf: main.tex *.tex $(LUATEX_CACHE)
	docker run $(DOCKERFLAGS) $(DOCKERIMAGE) sh -c '$(TEX_SCRIPT)'
else
$(PROJECT_NAME).pdf: main.tex *.tex
	$(TEX_SCRIPT)
endif

.PHONY: clean

clean:
	-@rm -r *.aux *.log *.nav *.out *.snm *.toc *.vrb _minted-* $(PROJECT_NAME).pdf
